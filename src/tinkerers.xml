<chapter id="tinkerers">
  <title>The Tinkerers</title>

  <para>
    Once they have found either a data cache about the Tinkerers on
    Yazid&#39;s body, or else have found information on the Tinkerers
    from malware embedded in Peter&#39;s home PC, players may attend a
    meeting of the Tinkerers at 99 Edwards Street in the sprawl.
  </para>

  <note id="gengrove-read">
    <title>Read this aloud to your players&#58;</title>

    <para>
      You approach a building much like all the others in the
      sprawl&#58; run-down and neglected, yet bolstered by
      wrought-iron bars over the windows and triple locks on the
      door. Spray-painted across the building in bright green paint
      are the words <emphasis>Slackers HQ</emphasis>.
    </para>

    <para> Right now, the building's front door is open, though, and a
    cardboard sign nailed to the outside wall states &#34;Community
    meeting today. Come one, come all. DIY workshops, free computer
    help, building project planning.&#34;
    </para>

    <para>
      Inside, fourteen community members have gathered, all huddled
      around computers or workbenches and powertools. A plate of stale
      biscuits and flavoured water sits near the entrance. Light
      streams out of the doorway into the dim, rainy streets. It&#39;s
      an inviting atmosphere, and one you&#39;re not entirely
      accustomed to.
    </para>
  </note>
    
  <para>
    The meeting is an open meeting. All are welcome. They do not
    openly advertise that they call themselves the Tinkerers; in fact,
    they say that the meeting is a gathering of people who want to
    work to rebuild the sprawl into a livable, corporate-free,
    self-governing neighbourhood.
  </para>

  <para>
    The workshop is filled with electronic and building equipment. It
    appears to be a sort of community workshop.
  </para>

  <para>
    There is a well-hidden secret room behind a shelf of electronics,
    where Peter Nazari is monitoring the meeting. This room can be
    detected with a <emphasis id="bold">perception DC 30</emphasis>
    check, but it is well-guarded by the attendees.
  </para>

  <para>
    The network within the workshop is military-grade and aggressive
    to intruders.
  </para>
  
  <para>
    If players speak to the people gathered there and mention the word
    &#34;tinkerers&#34;, they will agree that they love to tinker on
    things, and they will allow the players to call them
    &#34;tinkerers&#34; if that&#39;s what the players want to
    do. It&#39;s the sprawl&#59; you can do whatever you want.
  </para>

  <para>
    The names of the Tinkerers are&#58; Sook,Sonny,Spike,Tam,
    Targa, Tarris, Uno, Uke, Ustan, Vic, Vlad, Volo,Wish, and
    Wendy. They all have the same stats&#58;
  </para>

  <tip id="tinkerer-hacker">
    <title>Hackers</title>
    <para>XP 600 &#40;CR 2&#41;</para>
    <para>Medium human</para>
    <para>Init +4; darkvision 60 ft., low-light vision; Perception +10</para>
    <para><emphasis role="bold">DEFENSE</emphasis></para>
    <para>DS 14 (+3 Dex, +1 natural); DR 2</para>
    <para>Critical Defense Bonus 5 (DR + Dex mod + shield bonus)</para>
    <para>hp 30 (5d6+10)</para>
    <para>Fort +3; Ref +4; Will +5; +2 vs psionics, +4 vs. mind-affecting,paralysis, stun</para>
    <para><emphasis role="bold">OFFENSE</emphasis></para>
    <para>Speed 30 ft.</para>
    <para>Melee UP Ironfang spring-blade +2 (1/2/4)</para>
    <para>Ranged HT-9 +5 (2/7/12)</para>
    <para><emphasis role="bold">STATISTICS</emphasis></para>
    <para>Str 10, Dex 16, Con 14, Int 16, Wis 12, Cha 8</para>
    <para>Base Atk +2; CMB +2; CMD 15</para>
    <para>TAP: Firewall 15; AMS 5; Neural Fort +4; Range 125 ft.</para>
    <para>Feats Cutter, Interference, Skill Focus (hacking)</para>
    <para>Skills Bluff +6, Drive +8, Hacking +14, Knowledge (Global
    DataNet) +11, Knowledge (local) +11, Knowledge (programming) +11,
    Perception +10, Sense Motive -3; Racial Modifiers -4 Sense Motive
    Traits Hacker, Seeker</para>
    <para>Contacts merchant, petty criminal</para>
    <para>Languages Cantonese, English, Japanese, Russian</para>
    <para>Gear HT-9 holdout pistol, UP Ironfang spring-blade, Hardened
    UP Hoodyz (light fortification)</para>
  </tip>

  <para>
    If the players mention Peter Nazari, the hackers show no sign of
    recognition, but will discuss Peter and Global Omnium Digital with
    great interest. They will guardedly entertain any idea of taking
    down the corporation, although they will pretend not to know who
    Peter is, where he is, or even that he is on their side.
  </para>

  <para>
    At the end of the meeting, as long as the players have given the
    Tinkeres <emphasis>any</emphasis> reason to trust them, they are
    invited to stay for the after-meeting meeting. Seven &#40;or more,
    if your APL is particularly low&#41; of the
    attendees leave, with seven plus the players remaining. At that
    point, Peter Nazari comes out of a back room that had been
    concealed behind a shelf of electronic equipment.
  </para>

  <section id="tinkerers-peter">
    <title>Peter Nazari</title>

    <para>
      Peter Nazari has joined the Tinkerers in an effort to bring down
      Global Omnium Digital, as he believes that Elizabeth Ravadun has
      been committing atrocities on her employees. He doesn&#39;t know
      exactly what she has been doing to them, but years ago, he
      started investigating the internal hospital system and found
      that sometimes people got checked into the system and never
      checked out.
    </para>

    <para>
      Peter believes he can run the corporation ethically, and
      believes the Tinkerers can help him take Elizabeth
      down. However, what they have in hacking skills, they lack in
      firepower. He offers to hire the players to help him storm the
      castle. He offers to match what Elizabeth is paying them.
    </para>

    <para>
      If the players accept, he tells them that they are ready to
      move in on Elizabeth, so he will get in touch with them later
      that night with details on where to be.
    </para>

    <para>
      If the players mention Tina, Peter will acknowledge that he
      was forced to leave her abruptly, and that he regrets it. He
      says that he cannot risk telling Tina what he is doing now, but
      that it will all get sorted out after he successfully takes over
      Global Omnium Digital, or dies trying.
    </para>
  </section>

  <section id="tinkerer-combat">
    <title>Tinkerer combat</title>
    
    <para>
      If the players decline Peter&#39;s offer of joining the Tinkerers, a
      battle ensues. And probably ends resolves the adventure.
    </para>      

    <tip id="peter-stats">
      <title>Peter Nazari</title>
      <para>XP 2600 &#40;CR 2&#41;</para>
      <para>Human 2.0 idol 5</para>
      <para>Medium humanoid (human)</para>
      <para>Init +7; Senses Perception -1</para>
      <para><emphasis role="bold">DEFENSE</emphasis></para>
      <para>DS 14; DR 6</para>
      <para>Critical Defense Bonus +9 (DR + Dex + shield)</para>
      <para>hp 42 (5d8+10)</para>
      <para>Fort +3; Ref +7; Will +3; +4 vs. disease</para>
      <para><emphasis role="bold">OFFENSE</emphasis></para>
      <para>Speed 30 ft.</para>
      <para>Melee combat knife +5 (3/5/10/ 19-20)</para>
      <para>Ranged AGA thunderbolt +6 (2/9/16)</para>
      <para><emphasis role="bold">STATISTICS</emphasis></para>
      <para>Str 14, Dex 16, Con 14, Int 13, Wis 8, Cha 18</para>
      <para>Base Atk +3; CMB +5; CMD 18</para>
      <para>Zero Points 8</para>
      <para>TAP: Firewall 14; AMS 4; Neural Fort +4; Range 50 ft.</para>
      <para>Feats Extra Celebrity, Improved Initiative, Point Blank Shot, Skill</para>
      <para>Focus (Perform [oration])</para>
      <para>Skills Acrobatics +8, Bluff +12, Diplomacy +12, Drive +11,
      Intimidate +12, Knowledge (local) +10, Perform (oration) +12, Stealth +11</para>
      <para>Traits Charming</para>
      <para>Drawbacks Arrogant</para>
      <para>Contacts rumormonger, hackers</para>
      <para>Languages English</para>
      <para>SQ arrogant, beautiful people, celebrity pool (6 points),
      command performance (fascinate), perks (do you know who I am?, hogging the spotlight),
      well-connected +2, who you know (2/day)</para>
      <para>Gear AGA Thunderbolt, UP Duke McCoy combat knife, Executive
      Decision armour suit
      </para>
    </tip>
    
    <para>
      If the players engage Peter and the Tinkerers in combat, then
      Peter retreats behind cover and calls Tina to beg for her
      help. The Tinkerers, in the mean time, attempt to hack into the
      players&#39;s personal networks to cause various kinds of
      technological damage, and also use any weapon at their disposal
      to protect themselves and Peter.
    </para>

    <para>
      Tina answers his call, arriving at the last minute, along with
      three thugs borrowed from one of her many powerful clients.
    </para>

    <tip id="ganger-stats">
      <para>Human ganger 5</para>
      <para>Medium humanoid (human)</para>
      <para>Init +3; Senses low-light vision; Perception +9</para>
      <para><emphasis role="bold">DEFENSE</emphasis></para>
      <para>Defense Score 13 (+3 Dex); DR 6</para>
      <para>Critical Defense Bonus +8 (DR + Dex + shield)</para>
      <para>hp 36 (5d8+10)</para>
      <para>Fort +2; Ref +9; Will +2</para>
      <para>Defensive Abilities&#58; evasion, uncanny dodge</para>
      <para><emphasis role="bold">OFFENSE</emphasis></para>
      <para>Speed 30 ft.</para>
      <para>Melee reinforced unarmed strike +7 (1/2/4+4) or combat knife
      +7 (1/3/6+4)</para>
      <para>Ranged PD9 +6 (2/5/8; semi-auto &#38; autofire)</para>
      <para>Special Attacks brutal beating, gang-fighting, gets the job
      done, neak attack +10</para>
      <para><emphasis role="bold">STATISTICS</emphasis></para>
      <para>Str 18, Dex 14, Con 12, Int 10, Wis 12, Cha 13</para>
      <para>Base Atk +3; CMB +6; CMD 18</para>
      <para>TAP: Firewall 14; AMS 4; Neural Fort +3; Range 25 ft.</para>
      <para>Feats Autofire Expert, Chromed, Intimidating Prowess,
      Lightning Reflexes, Power Attack, Weapon Focus
      (submachinegun)</para>
      <para>Skills Acrobatics +10, Appraise +8, Drive +10, Knowledge (local)
      +8, Intimidate +12, Perception +9, Sense Motive +9, Sleight of
      Hand +10, Stealth +10</para>
      <para>Traits Dirty Fighter</para>
      <para>SQ frightening, rogue talent (strong impression)
      Cyberware cybereyes w/ nightvision optics, street warrior
      gutterware package</para>
      <para>Gear RS PD9 submachinegun, UP Duke McCoy combat knife,
      RW reinforced biker jacket</para>
      <para>Frightening (Ex): Intimidation duration lasts for 1 extra round.</para>
      <para>Sneak Attack</para>
    </tip>

    <para>
      Tina and her gangers will fight to the death to defend Peter.
    </para>

    <para>
      Peter will also fight to the death, but he can be disarmed or
      disabled.
    </para>

    <para>
      If the players turn Peter in, Elizabeth Ravadun pays them as
      agreed. She also reveals, if prompted, that Peter had permitted
      doctors within Global Omnium Digital to experiment on patients
      as a means of reducing research costs &#40;ethics, it turns out,
      are expensive&#41;. She believes that he was going to blame her
      for it.
    </para>

    <para>
      If asked about Yazid, Elizabeth will not bother denying having
      him executed, and simply explains that she couldn&#39;t afford
      to let rogue hackers running loose in her company. Yazid had dug
      deep for her, but she fears that if he was able to hijack one
      board member&#39;s email accounts and cover his tracks, he could
      do the same to anyone else. So she disposed of him for safety.
    </para>

    <para>
      Should the players decide to attack Elizabeth, fast forward to
      the final encounter with Elizabeth.
    </para>
  </section>
</chapter>
