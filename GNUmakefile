build:
	mkdir $@

help:
	@echo "make concat   Concatenates files."
	@echo "make html     Generate book as HTML."
	@echo "make txt      Generate book as plain text."
	@echo "make epub     Generate book as an epub."
	@echo "make pdf      Generate book as an PDF."
	@echo "make clean    Remove temp files."

coverp:	img/cover_front.jpg
	@convert img/cover_front.jpg build/front.pdf

concat: src/header.xml src/footer.xml build
	@test -d build || mkdir build
	cat src/header.xml \
	src/preface.xml \
	src/backstory.xml \
	src/fixer.xml \
	src/building2019.xml \
	src/penthouse.xml \
	src/next.xml \
	src/tina.xml \
	src/gengrove.xml \
	src/tinkerers.xml \
	src/flame.xml \
	src/corporate.xml \
	src/email.xml \
	src/drop.xml \
	src/encalc.xml \
	src/OGLv1.0a.xml \
	src/colophon.xml \
	src/footer.xml > tmp.xml

html:	build concat
	@mkdir dist  || true
	xmlto --skip-validation -o build html-nochunks tmp.xml
	@mv build/tmp.html dist/jobinsecurity.html

txt:	build concat
	@mkdir dist  || true
	xmlto --skip-validation -o build txt tmp.xml
	@mv build/tmp.txt dist/jobinsecurity.txt

epub:	build concat
	@mkdir build || true
	@mkdir dist  || true
	xmlto --skip-validation -o build epub tmp.xml
	@mv build/tmp.epub dist/jobinsecurity.epub

pdf:	build concat coverp
	@mkdir build || true
	@mkdir dist  || true
	xsltproc --output build/tmp.fo \
	 --stringparam paper.type  A4 \
	 --stringparam page.width 8in \
	 --stringparam page.height 10in \
	 --stringparam my.guild.logo "../img/redline.svg" \
	 --stringparam redist.text "sa" \
	 --stringparam column.count.titlepage 1 \
	 --stringparam column.count.lot 1 \
	 --stringparam column.count.front 1 \
	 --stringparam column.count.body 2 \
	 --stringparam column.count.back 1 \
	 --stringparam column.count.index 2 \
	 --stringparam body.font.family "TeX Gyre Bonum" \
	 --stringparam title.font.family "Andada" \
	 --stringparam bridgehead.font.family "Junction" \
	 --stringparam symbol.font.family "UniCons" \
	 --stringparam footer.column.widths "1 0 0" \
	 --stringparam body.font.master 10 \
	 --stringparam body.font.size 10 \
	 --stringparam page.margin.inner .6in \
	 --stringparam page.margin.outer .6in \
	 --stringparam page.margin.top .45in \
	 --stringparam page.margin.bottom .45in \
	 --stringparam title.margin.left 0 \
	 --stringparam title.start.indent 0 \
	 --stringparam body.start.indent 0 \
	 --stringparam chapter.autolabel 0 \
	style/mystyle.xsl tmp.xml
	fop -c style/rego.xml build/tmp.fo build/tmp.pdf
	pdftk A=build/tmp.pdf B=build/front.pdf cat B A2-end output dist/jobinsecurity.pdf || mv build/tmp.pdf dist/jobinsecurity.pdf

clean:	build
	@rm -rf build
	@rm -rf tmp*xml
