# jobinsecurity

*A 2 to 4 hour adventure for Level 1 to 3 characters.*

This is a short adventure for the  Interface Zero or Starfinder varieties of the [Pathfinder](http://paizo.com/pathfinderRPG/) rules, which is a tabletop role-playing game (RPG). Since Pathfinder is a fork of **Dungeons and Dragons**, this adventure is easy to adapt for the latest edition of [D&D](http://dnd.wizards.com/), as well. 


## Elevator pitch

Your fixer sends you a message: a local CEO is worried that a board member is planning to remove her, and wants your team to ensure she keeps her job. No holds barred, by any means necessary. Seems simple, so why are this board member so hard to find?


## How do I build this?

What you are looking at right now is the source code for the adventure. If you want to download and play this adventure, it's much easier to just download a copy from http://www.drivethrurpg.com/product/229831.

### No really, I want to build this.

If, for whatever reason, you want to build this thing from source, you can do that. This was written on Slackware Linux and I've lazily structured it around Slackware's default Docbook install. If you are familiar with XML and Docbook, you should have no problem.

The build requires:

* [Docbook](http://docbook.org)
* [Junction](https://www.theleagueofmoveabletype.com/junction) font
* [Andada](http://www.1001fonts.com/andada-font.html)
* [TeX Gyre Bonum](http://www.1001fonts.com/tex-gyre-bonum-font.html) (bundled in this repo to fix render errors)
* [xmlto](https://pagure.io/xmlto)
* [xsltproc](http://xmlsoft.org/XSLT/index.html)
* [FOP 2](https://xmlgraphics.apache.org/fop/)
* [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)
* [Image Magick](http://imagemagick.org/script/index.php)

Optional, to avoid warnings about not having a **symbol** font installed:

* [UniCons](https://fontlibrary.org/en/font/unicons)

These are the steps I would take to do the build, were I you:

1. Clone this repo
2. Verify the Docbook paths in the ``header.xml`` file are appropriate for your system.
3. Verify the font paths in ``style/rego.xml`` are correct.
4. If you have a front cover image, verify that it exists and is in the appropriate location. If not, modify the makefile to remove ``coverp`` and ``pdftk`` from the PDF build process.
5. ``make clean concat pdf``
6. Find the PDF in the ``dist`` directory.

## License

It's all open source and free culture. See LICENSE file for details.